#什么是eleme_union_meal
------------------------
eleme_union_meal是一个B/S结构的软件，它能收集用户在饿了么下的订单，计算出最优惠的价格下单

# docker部署方式方式:
1. 安装docker和docker-compose
2. 修改server.json中的server_host
3. 进入项目根目录docker-compose build
4. 进入项目根目录docker-compose up

# 普通安装eleme_union_meal
--------------------
##first. linux系统下需要的安装包
* jansson (在uwsgi之前安装，确保uwsgi可以使用json格式的配置文件)
* uwsgi 
* iojs
* nginx
* redis
* python2.7+

##second. iojs需要的安装包
* jsdom
* redis
* request
* connect

##third. python需要的安装包
* bottle
* redis

##fourth. 配置nginx
创建一个eleme_union_nginx.conf的文件并把以下内容复制进去

    server {
        listen     8888;
        server_name    eleme_union;
        location ~ \.(html|js|css)$ {
            root /home/ele.me/client;
        }
        location ^~ /api/py/ {
            proxy_pass http://127.0.0.1:9000;
        }
        location ^~ /api/js/ {
            proxy_pass http://127.0.0.1:9001;
        }
    }

然后在编辑nginx的主配置文件增加下面一段include

    
    http {
        #...
        include /your/conf/dir/eleme_union_nginx.conf;
    }

##fifth. 修改server.json 文件

    {
    	"redis":{
    		"max_connections": 5,
    		"host": "127.0.0.1",
    		"port": 6379
    	},
    	"client_replace": {
    		"server_host": "http://192.168.56.104:8888",
    		"alipay_url": "https://qr.alipay.com/5734546795641850",
    		"qrcode_tips": "付费"
    	},
        "uwsgi": {
            "wsgi-file": "portal.py",
            "http-socket": "0.0.0.0:9000"
        },
    	"nodejs_port": 9001
    }

注意事项
* client_replace.server_host 必须是客户端能访问到的服务器地址，不能填127.0.0.1之类的
* uwsgi.http-socket 和 nodejs_port 必须与上面nginx.conf中的端口对应

##sixth. 运行服务
1. 进入eleme_union_meal/server 目录
2. 执行脚本 uwsgi --json ../server.json & iojs portal.js &

##seventh. 在客户端测试
1. 将以下js制作成书签保存到收藏夹中 `javascript:(function(){var f=document.createElement('script');f.src='http://192.168.56.104:8888/import.js';document.body.appendChild(f);})();`
2. 进入某个饿了么餐厅比如 http://r.ele.me/gz-zwg 然后点击那个js书签，这时会看到网页的右下角多了一个`输入框`和一个`买买买`按钮，
在输入框中输入你的名字
3. 选好你想买的菜单点击`买买买`按钮
4. 在跳转的网页中就可以看到你刚选的菜单了
5. 在大家下好单后点击该网页中的`锁`按钮，然后服务器就可以自动计算出最优惠订单分类，然后你只要根据分好类的订单在饿了么下单就行了




